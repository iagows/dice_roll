import DiceBox from "../lib/components/DiceBox";
import RollButton from "../lib/components/RollButton";
import { BoxProvider } from "../lib/context";

import "./App.css";

const colors: string[] = [
	"#ffffff",
	"#0f7f12",
	"#fffd38",
	"#fd9827",
	"#fc0d1b",
	"#376bfb",
	"#343797",
	"#c0c0c0",
];

function App() {
	return (
		<div className="App">
			<div id="dice-box">
				<BoxProvider
					htmlConfig={{
						background: "purple",
						height: "40vh",
						width: "40vw",
					}}
				>
					<DiceBox id="dice-box-element">
						<RollButton />
					</DiceBox>
				</BoxProvider>
			</div>
		</div>
	);
}

export default App;
