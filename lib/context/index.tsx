import DiceBoxThree from "@3d-dice/dice-box-threejs";
import { PropsWithChildren, createContext, useState } from "react";
import {
	ColorSetMaterial,
	HTMLConfig,
	Options,
	Textures,
} from "../types/Option";

const initialConfig: Options = {
	theme_customColorset: {
		background: "#808080",
		foreground: "#ffffff",
		texture: Textures.MARBLE,
		material: ColorSetMaterial.WOOD,
	},
	sounds: false,
	volume: 100,
	light_intensity: 1,
	gravity_multiplier: 300,
	baseScale: 120,
	strength: 2,
	onRollComplete: (results) => {
		console.log(`I've got results :>> `, results);
	},
};

type ContextType = {
	box: DiceBoxThree | null;
	setBox: (data: DiceBoxThree) => void;
	config: Options;
	setConfig: (data: Options) => void;
	htmlConfig: HTMLConfig;
};

export const BoxContext = createContext<ContextType>({
	box: null,
	setBox: () => {},
	config: initialConfig,
	setConfig: () => {},
	htmlConfig: {
		width: "500px",
		height: "500px",
		background: "black",
	},
});

type Config = {
	options?: Options;
	htmlConfig?: HTMLConfig;
};

export const BoxProvider = ({
	children,
	options,
	htmlConfig = {
		width: "500px",
		height: "500px",
		background: "black",
	},
}: PropsWithChildren<Config>) => {
	const [box, setBox] = useState<DiceBoxThree>(null);
	const [config, setConfig] = useState<Options>(initialConfig);

	return (
		<BoxContext.Provider
			value={{
				box,
				setBox,
				config: {
					...config,
					...options,
				},
				setConfig,
				htmlConfig,
			}}
		>
			{children}
		</BoxContext.Provider>
	);
};
