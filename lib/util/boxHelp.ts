import { ColorSetMaterial, Options, Textures } from "../types/Option";
import { getRandomFrom } from "./rand";
import DiceBoxThree from "@3d-dice/dice-box-threejs";

export async function setUpDiceBox(id: string, options: Options): DiceBoxThree {
	const box = new DiceBoxThree(`#${id}`, options);
	await box.initialize();
	return box;
}

export async function rollAgain(
	box: DiceBoxThree,
	amount: number,
	sides: number,
	color: string,
) {
	// all dice will produce the same value picked from the values list randomly
	const values = Array.from({ length: sides }, (_, i) => i + 1);
	const randomVal = getRandomFrom(values);

	const nextConfig: Options = {
		theme_customColorset: {
			background: color,
			foreground: "#ffffff",
			texture: Textures.MARBLE,
			material: ColorSetMaterial.WOOD,
		},
	};

	box.updateConfig(nextConfig);
	await box.roll(`${amount}d${sides}@${randomVal}`);
}

export function changeDiceColor(box: DiceBoxThree): void {
	console.log({ box });
}
