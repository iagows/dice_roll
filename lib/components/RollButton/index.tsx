import useRoll from "./useRoll";

const RollButton = () => {
	const { roll, showButton } = useRoll();

	return (
		<>{showButton && <input type="button" value={"Rolar"} onClick={roll} />}</>
	);
};

export default RollButton;
