import { useContext } from "react";
import { rollAgain } from "../../util/boxHelp";
import { BoxContext } from "../../context";
// import * as THREE from "three";

type Out = {
	roll: () => void;
	showButton: boolean;
};

const colors: string[] = [
	"0x0f7f12",
	"0xffffff",
	"0xfffd38",
	"0xfd9827",
	"0xfc0d1b",
	"0x376bfb",
	"0x343797",
	"0xc0c0c0",
];

const useRoll = (): Out => {
	const { box } = useContext(BoxContext);

	const roll = async () => {
		await rollAgain(box, 1, 20, "#222");

		const dices = box.diceList;
		const dice = dices[0];

		console.log({ dice });

		for (const m of dice.material) {
			m.color.setHex(colors[4]);
			m.needsUpdate = true;
		}
		dice.geometry.elementsNeedUpdate = true;

		box.renderer.render(box.scene, box.camera);
	};

	return {
		roll,
		showButton: box !== null,
	};
};

export default useRoll;
