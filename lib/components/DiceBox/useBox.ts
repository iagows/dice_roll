import { useContext } from "react";
import { setUpDiceBox } from "../../util/boxHelp";
import { BoxContext } from "../../context";
import { HTMLConfig } from "../../types/Option";

type Out = {
	init: (id: string, boxRef: React.RefObject<HTMLDivElement>) => Promise<void>;
	htmlConfig: HTMLConfig;
};

const useBox = (): Out => {
	const { config, setBox, htmlConfig } = useContext(BoxContext);

	const init = async (id: string, boxRef: React.RefObject<HTMLDivElement>) => {
		if (boxRef.current) {
			// check if there's a canvas in the #scene-container
			const hasCanvas = Array.from(boxRef.current.children).filter(
				(child) => child.tagName === "CANVAS",
			);
			if (hasCanvas.length === 0) {
				const box = await setUpDiceBox(id, config);
				setBox(box);
			}
		}
	};

	return {
		init,
		htmlConfig,
	};
};

export default useBox;
