import { PropsWithChildren, useEffect, useRef } from "react";
import useBox from "./useBox";

type Props = {
	id: string;
};

const DiceBox = ({ children, id }: PropsWithChildren<Props>) => {
	const boxRef = useRef<HTMLDivElement>(null);

	const { init, htmlConfig } = useBox();

	useEffect(() => {
		init(id, boxRef);
	}, [id, init]);

	return (
		<div style={htmlConfig} ref={boxRef} id={id}>
			{children}
		</div>
	);
};

export default DiceBox;
