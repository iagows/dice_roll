type Rolls = {
	id: number;
	label: string;
	reason: string;
	sides: number;
	type: string;
	value: number;
};

type Sets = {
	num: number;
	rolls: Rolls[];
	sides: number;
	total: number;
	type: string;
};

export enum Textures {
	MARBLE = "marble",
	ICE = "ice",
}

type Results = {
	modifier: number;
	notation: string;
	total: number;
	sets: Sets[];
};

enum Materials {
	NONE = "none",
	METAL = "metal",
	WOOD = "wood",
	GLASS = "glass",
	PLASTIC = "plastic",
}

export enum ColorSetMaterial {
	METAL = "metal",
	GLASS = "glass",
	PLASTIC = "plastic",
	WOOD = "wood",
}

export type Options = {
	theme_customColorset: {
		background: string;
		foreground: string;
		texture: Textures;
		material: ColorSetMaterial;
	};
	sounds?: boolean;
	volume?: number;
	light_intensity?: number;
	gravity_multiplier?: number;
	baseScale?: number;
	strength?: number;
	onRollComplete?: (result: Results) => void;
	framerate?: number;
	color_spotlight?: number;
	shadows?: true;
	theme_surface?: string;
	sound_dieMaterial?: string;
	theme_colorset?: string; // see available colorsets in https://github.com/3d-dice/dice-box-threejs/blob/main/src/const/colorsets.js
	theme_texture?: string; // see available textures in https://github.com/3d-dice/dice-box-threejs/blob/main/src/const/texturelist.js
	theme_material?: Materials;
};

export type HTMLConfig = {
	background?: string;
	width?: number | string;
	height?: number | string;
};
